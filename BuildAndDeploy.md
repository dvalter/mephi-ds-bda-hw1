Build and deploy manual
=======================

## Required tools

- docker
- git
- non-root access to docker (i.e. user membership in `docker` group on Linux_

## Preparation

Download sources

```
git clone https://bitbucket.org/dvalter/mephi-ds-bda-hw1/src/master/
git submodule update --init
```

Build docker container

```
cd mephi-ds-bda-hw1/docker
./build-image.sh
cd ..
```

## Deploy

### Prepare containers

Start docker containers and hadoop

```
cd docker
./start-container.sh
```

You shold receive the following output and get into master's shell


Test on word count

```
/root/run-wordcount.sh
```

You shold receive the following output

```
input file1.txt:
Hello Hadoop

input file2.txt:
Hello Docker

wordcount output:
Docker    1
Hadoop    1
Hello    2
```

### Build jar file


```
mvn package
```

Alternatively you may run this on the host system if you have JDK and maven there

### Test / Run

Generate test file and copy it to HDFS

```
./generate_data.sh data 7 10000
```

It will produce a test file with some data being intentially corrupted

Alternatively : use your own input data

To put input data to HDFS you may use

```
hdfs dfs -rm -r /user/root/input
hdfs dfs -mkdir -p /user/root
hdfs dfs -put input /user/root
```

Run bytes per IP extraction job

```
hadoop jar ./target/hw1-1.0-SNAPSHOT-jar-with-dependencies.jar input output
```

Check the result

```
hdfs dfs -cat output/part-r-00000
```
