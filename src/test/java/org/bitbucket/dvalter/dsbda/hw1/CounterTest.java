package org.bitbucket.dvalter.dsbda.hw1;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class CounterTest {
    private MapDriver<Object, Text, Text, IntPairWritable> mapDriver;

    private final static String validLine = "::1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0\"";
    private final static String malformedLine = "1.1.1.1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 40028 \"-\" \"Tormazila/5.0 (X80; Xenix x86_16; rv:68.0) PaleMoon/68.0\"";
    private final static String malformedHostLine = "localhost - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 40028 \"-\" \"Tormazila/5.0 (X80; Xenix x86_16; rv:68.0) PaleMoon/68.0\"";

    @Before
    public void init() {
        BytesPerIP.HttpdBytesMapper mapper = new BytesPerIP.HttpdBytesMapper();
        mapDriver = new MapDriver<>(mapper);
    }

    @Test
    public void singleValidHost() throws IOException {
        mapDriver
                .withInput(NullWritable.get(), new Text(validLine))
                .withOutput(new Text("::1"), new IntPairWritable(40028, 1))
                .runTest();
        assertEquals("Excepted no increment", 0,
                mapDriver.getCounters().findCounter(BytesPerIP.CounterType.MALFORMED).getValue());
    }

    @Test
    public void singleMalformedHost() throws IOException {
        mapDriver
                .withInput(NullWritable.get(), new Text(malformedLine))
                .runTest();
        assertEquals("Excepted increment by 1", 1,
                mapDriver.getCounters().findCounter(BytesPerIP.CounterType.MALFORMED).getValue());
    }

    @Test
    public void multipleHosts() throws IOException {
        mapDriver
                .withInput(NullWritable.get(), new Text(malformedLine))
                .withInput(NullWritable.get(), new Text(malformedLine))
                .withInput(NullWritable.get(), new Text(validLine))
                .withInput(NullWritable.get(), new Text(malformedHostLine))
                .withOutput(new Text("::1"), new IntPairWritable(40028, 1))
                .runTest();
        assertEquals("Excepted increment by 3", 3,
                mapDriver.getCounters().findCounter(BytesPerIP.CounterType.MALFORMED).getValue());
    }

}
