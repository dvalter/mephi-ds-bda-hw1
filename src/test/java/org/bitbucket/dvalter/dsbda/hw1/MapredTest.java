package org.bitbucket.dvalter.dsbda.hw1;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapredTest {
    private MapDriver<Object, Text, Text, IntPairWritable> mapDriver;
    private ReduceDriver<Text, IntPairWritable, Text, IntDoubleWritable> reduceDriver;
    private ReduceDriver<Text, IntPairWritable, Text, IntPairWritable> combinerDriver;
    private MapReduceDriver<Object, Text, Text, IntPairWritable, Text, IntDoubleWritable> mapReduceDriver;

    private final static String host = "1.1.1.1";
    private final static String testLine = "1.1.1.1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0\"";
    private final static String testLine2 = "1.1.1.1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 2 \"-\" \"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0\"";

    @Before
    public void init() {
        BytesPerIP.HttpdBytesMapper mapper = new BytesPerIP.HttpdBytesMapper();
        BytesPerIP.SumAvgReducer reducer = new BytesPerIP.SumAvgReducer();
        BytesPerIP.SumCountCombiner combiner = new BytesPerIP.SumCountCombiner();
        mapDriver = new MapDriver<>(mapper);
        reduceDriver = new ReduceDriver<>(reducer);
        combinerDriver = new ReduceDriver<>(combiner);
        mapReduceDriver = new MapReduceDriver<>(mapper, reducer);
        mapReduceDriver.setCombiner(combiner);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(NullWritable.get(), new Text(testLine))
                .withOutput(new Text(host), new IntPairWritable(40028, 1))
                .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<IntPairWritable> values = new ArrayList<>();
        values.add(new IntPairWritable(2, 1));
        values.add(new IntPairWritable(3, 1));
        reduceDriver
                .withInput(new Text(host), values)
                .withOutput(new Text(host), new IntDoubleWritable(5, 2.5))
                .runTest();
    }

    @Test
    public void testCombiner() throws IOException {
        List<IntPairWritable> values = new ArrayList<>();
        values.add(new IntPairWritable(2, 1));
        values.add(new IntPairWritable(3, 1));
        combinerDriver
                .withInput(new Text(host), values)
                .withOutput(new Text(host), new IntPairWritable(5, 2))
                .runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(NullWritable.get(), new Text(testLine))
                .withInput(NullWritable.get(), new Text(testLine2))
                .withOutput(new Text(host), new IntDoubleWritable(40030, 20015))
                .runTest();
    }
}
