package org.bitbucket.dvalter.dsbda.hw1;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

/**
 * Hadoop-compatible class for a pair of integer and double values
 */
public class IntDoubleWritable implements Writable {
    private final IntWritable value1;
    private final DoubleWritable value2;

    /**
     * Constructor
     */
    public IntDoubleWritable() {
        value1 = new IntWritable();
        value2 = new DoubleWritable();
    }

    /**
     * Constructor
     *
     * @param value1 integaer value
     * @param value2 double value
     */
    public IntDoubleWritable(int value1, double value2) {
        this.value1 = new IntWritable(value1);
        this.value2 = new DoubleWritable(value2);
    }

    /**
     * Getter
     *
     * @return integer value
     */
    public int getInt() {
        return value1.get();
    }

    /**
     * Getter
     *
     * @return double value
     */
    public double getDouble() {
        return value2.get();
    }

    /**
     * String convertor
     *
     * @return string representation, comma-separated
     */
    @Override
    public String toString() {
        return String.format("%d,%.3f", value1.get(), value2.get());
    }

    /**
     * Read from data input
     *
     * @param in DataInput to read from
     * @throws IOException
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        value1.readFields(in);
        value2.readFields(in);
    }

    /**
     * Write to data output
     *
     * @param out DataOutput to write to
     * @throws IOException
     */
    @Override
    public void write(DataOutput out) throws IOException {
        value1.write(out);
        value2.write(out);
    }

    /**
     * Comparator
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntDoubleWritable that = (IntDoubleWritable) o;
        return Objects.equals(value1, that.value1) &&
                Objects.equals(value2, that.value2);
    }

    /**
     * Hash Code
     */
    @Override
    public int hashCode() {
        return Objects.hash(value1, value2);
    }
}
