package org.bitbucket.dvalter.dsbda.hw1;

import com.google.common.net.InetAddresses;
import nl.basjes.parse.core.Field;
import nl.basjes.parse.core.Parser;
import nl.basjes.parse.httpdlog.HttpdLoglineParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.IOException;

/**
 * Main class of the Hadoop application
 * <p>
 * Finds and counts data transmitted to peers in httpd-like web server logs by peer IP address
 */
public class BytesPerIP {

    /**
     * User counter for malformed lines in the log
     */
    public enum CounterType {
        MALFORMED
    }


    public static class LogEntryRecord {
        private String host;
        private int bytes;

        @Field("IP:connection.client.host")
        public void setIP(final String value) {
            host = value;
        }

        @Field("BYTESCLF:response.body.bytes")
        public void setBytes(final String value) {
            bytes = Integer.parseInt(value);
        }

        public String getHost() {
            return host;
        }

        public int getBytes() {
            return bytes;
        }
    }

    /**
     * Mapper class
     * <p>
     * Parses the log line and outputs IP address and number of bytes transmitted
     */
    public static class HttpdBytesMapper extends Mapper<Object, Text, Text, IntPairWritable> {

        /**
         * Log format according to the given references
         */
        private static final String logFormat = "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"";

        /**
         * Statically allocated Hadoop text for map() output
         * <p>
         * for JVM memory usage optimizations
         */
        private final Text ipAddress = new Text();

        /**
         * Statically allocated class for the log line parser output
         */
        private final LogEntryRecord record = new LogEntryRecord();

        /**
         * Statically allocated log parser
         */
        private final Parser<LogEntryRecord> httpdParser = new HttpdLoglineParser<>(LogEntryRecord.class, logFormat);

        /**
         * Map method
         * <p>
         * Parses IP and data size from the log line and counts malformed ones
         *
         * @param key     Key value of MapReduce input, unused
         * @param value   Input text, log line
         * @param context MapReduce job context
         */
        @Override
        protected void map(Object key, Text value, Context context) {
            // Convert input line to string
            String line = value.toString();
            try {
                // Parse log line
                httpdParser.parse(record, line);

                // Check if the host is a valid address
                if (InetAddresses.isInetAddress(record.getHost())) {
                    // Pass it to reducer
                    ipAddress.set(record.getHost());
                    context.write(ipAddress, new IntPairWritable(record.getBytes(), 1));
                } else {
                    // Count line as malformed if there's no IP address
                    context.getCounter(CounterType.MALFORMED).increment(1);
                }
            } catch (Exception e) {
                // Count line as malformed if it can't be parsed
                context.getCounter(CounterType.MALFORMED).increment(1);
            }
        }
    }

    /**
     * Reducer class
     * <p>
     * Counts total and average amount of data per host
     */
    public static final class SumCountCombiner extends Reducer<Text, IntPairWritable, Text, IntPairWritable> {

        /**
         * Reduce method
         *
         * @param key     MapReduce key, IP address
         * @param values  MapReduce value, list of occurrences and data sizes for the key
         * @param context MapReduce context
         * @throws IOException
         * @throws InterruptedException
         */
        @Override
        protected void reduce(Text key, Iterable<IntPairWritable> values, Context context) throws IOException, InterruptedException {
            int total = 0;
            int count = 0;
            // Sums data and counts IP address occurrences
            while (values.iterator().hasNext()) {
                IntPairWritable pair = values.iterator().next();
                total += pair.getInt1();
                count += pair.getInt2();
            }
            // Pass calculated sum and average to the output
            context.write(key, new IntPairWritable(total, count));
        }
    }

    /**
     * Reducer class
     * <p>
     * Counts total and average amount of data per host
     */
    public static final class SumAvgReducer extends Reducer<Text, IntPairWritable, Text, IntDoubleWritable> {

        /**
         * Reduce method
         *
         * @param key     MapReduce key, IP address
         * @param values  MapReduce value, list of occurrences and data sizes for the key
         * @param context MapReduce context
         * @throws IOException
         * @throws InterruptedException
         */
        @Override
        protected void reduce(Text key, Iterable<IntPairWritable> values, Context context) throws IOException, InterruptedException {
            int total = 0;
            int count = 0;
            // Sums data and counts IP address occurrences
            while (values.iterator().hasNext()) {
                IntPairWritable pair = values.iterator().next();
                total += pair.getInt1();
                count += pair.getInt2();
            }
            // Pass calculated sum and average to the output
            context.write(key, new IntDoubleWritable(total, count != 0 ? (double) total / count : 0));
        }
    }

    /**
     * MapReduce job entry point
     *
     * @param args Command line arguments
     * @throws Exception Possible exceptions from various MapReduce processes
     */
    public static void main(String[] args) throws Exception {
        // Create new configuration
        Configuration conf = new Configuration();
        // Output should match RFC 4180 "Common Format and MIME Type for Comma-Separated Values (CSV) Files"
        // Since IP addresses may not contain quotes and commas, it may be accomplished by adding
        // comma separator
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        // Create a job named "Bytes per IP" for configuration
        Job job = Job.getInstance(conf, "Bytes per IP");
        // Set main MapReduce class to UACount.class
        job.setJarByClass(BytesPerIP.class);
        // Set mapper class to HttpdBytesMapper.class
        job.setMapperClass(HttpdBytesMapper.class);
        // Set Combiner class to SumCountCombiner.class
        job.setCombinerClass(SumCountCombiner.class);
        // Set Reducer class to SumAvgReducer.class
        job.setReducerClass(SumAvgReducer.class);
        // Set output key class to text
        job.setOutputKeyClass(Text.class);
        // Set output value class to IntPairWritable
        job.setMapOutputValueClass(IntPairWritable.class);
        // Result words will be each be printed on a new row

        // Set input and output paths to paths provided in command line arguments
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        // Wait for completion of the job
        if (job.waitForCompletion(true)) {
            // If job succeeded, print user defined counter's values and exit with success
            System.out.println("Amount of malformed log lines: " + job.getCounters().findCounter(CounterType.MALFORMED).getValue());
            System.exit(0);
        }
        // If job failed, and exit with failure
        System.exit(1);
    }
}
