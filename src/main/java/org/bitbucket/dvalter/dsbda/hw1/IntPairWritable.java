package org.bitbucket.dvalter.dsbda.hw1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

/**
 * Hadoop-compatible class for a pair of integer values
 */
public class IntPairWritable implements Writable {
    private final IntWritable value1;
    private final IntWritable value2;

    /**
     * Constructor
     */
    public IntPairWritable() {
        this.value1 = new IntWritable();
        this.value2 = new IntWritable();
    }

    /**
     * Constructor
     *
     * @param value1 first value
     * @param value2 second value
     */
    public IntPairWritable(int value1, int value2) {
        this.value1 = new IntWritable(value1);
        this.value2 = new IntWritable(value2);
    }

    /**
     * Getter
     *
     * @return first value
     */
    public int getInt1() {
        return value1.get();
    }

    /**
     * Getter
     *
     * @return second value
     */
    public int getInt2() {
        return value2.get();
    }

    /**
     * String convertor
     *
     * @return string representation, comma-separated
     */
    @Override
    public String toString() {
        return value1.toString() + "," + value2.toString();
    }

    /**
     * Read from data input
     *
     * @param in DataInput to read from
     * @throws IOException
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        value1.readFields(in);
        value2.readFields(in);
    }

    /**
     * Write to data output
     *
     * @param out DataOutput to write to
     * @throws IOException
     */
    @Override
    public void write(DataOutput out) throws IOException {
        value1.write(out);
        value2.write(out);
    }

    /**
     * Comparator
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntPairWritable that = (IntPairWritable) o;
        return Objects.equals(value1, that.value1) &&
                Objects.equals(value2, that.value2);
    }

    /**
     * Hash Code
     */
    @Override
    public int hashCode() {
        return Objects.hash(value1, value2);
    }
}
