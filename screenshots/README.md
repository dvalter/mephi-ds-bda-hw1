Screenshots
===========

## Screenshot of successfully executed test

![Mapper and reducer tests with maven](./ScreenshotOfSuccessfullyExecutedTests.png)

## Screenshot of successfully uploaded file into HDFS

![Screenshot of successfully uploaded file into HDFS](./ScreenshotOfSuccessfullyUploadedFileIntoHDFS.png)

## Screenshot of successfully executed job and result

![Screenshot of successfully executed job and result](./ScreenshotOfSuccessfullyExecutedJobAndResult.png)

## Screenshot with information about memory consumption

### When idle

![Usage when idle](./UsageStale.png)

### During Map stage

![Usage during Map](./UsageMap.png)

### During Reduce stage

![Usage during Map](./UsageReduce.png)
