#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo 'You should specify output file!'
    exit 1
fi

NFILES=${2:-5}
NLINES=${3:-200}

# write some really existing User Agents and one malformed
USER_AGENTS=("Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko" 
            "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0;  rv:11.0) like Gecko" 
            "Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0" 
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0"
            "Opera/9.80 (X11; Linux i686; U; es-ES) Presto/2.8.131 Version/11.11"
            "Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.1) Gecko/20061208 Firefox/5.0 Opera 11.11"
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-gb) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; sv-se) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
            "Mozilla/5.0 Bad Agent\" - - - - ")
IP_ADDRESSES=("::"
            "127.0.0.1"
            "::5"
            "c0d3::1:1:1111"
            "1.2.3.4"
            "192.168.10.11"
            "132.56.9.113"
            "1.1.1.1"
            "53.11.3.7"
            "10.0.0.1"
            "fe80::ffdc:ee11:10c1"
            ":"
            "-")
# prefix doesn't mean something - it can be repeatable
PREFIX="- - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200"
SUFFIX="\"-\" \""

rm -rf input
mkdir input

for j in $(seq 1 $NFILES )
do
    for i in $(seq 1 $NLINES )
    do 
        IP="${IP_ADDRESSES[$((RANDOM % ${#IP_ADDRESSES[*]}))]}"
        BYTES=$((RANDOM*1024+RANDOM%1024))
        RESULT="$IP $PREFIX $BYTES $SUFFIX${USER_AGENTS[$((RANDOM % ${#USER_AGENTS[*]}))]}\""
        echo "$RESULT" >> input/$1.$j
    done
done

hdfs dfs -rm -r /user/root/input
hdfs dfs -mkdir -p /user/root
hdfs dfs -put input /user/root
